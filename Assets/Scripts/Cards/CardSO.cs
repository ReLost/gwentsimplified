using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Gwent Card", menuName = "GwentSimplified/New Card", order = 1)]
public class CardSO : ScriptableObject
{
    [Space(5)]
    public string ID;
    
    public string nameInGame;

    public EArmy army;

    public ECardType cardType;
    
    [InlineEditor(InlineEditorModes.LargePreview)]
    public Sprite sprite;

    [Multiline]
    [Tooltip("Visible on a tooltip")]
    public string description;
    
    public int health;
    
    [ShowIf("cardType", ECardType.DPS)]
    public int damage;

    [ShowIf("cardType", ECardType.HEALER)]
    public int healValue;

    public string GetFormattedDescription()
    {
        switch (cardType)
        {
            case ECardType.DPS:
                return string.Format(description, damage);
            case ECardType.HEALER:
                return string.Format(description, healValue);
            default:
                return description;
        }
    }
}

public enum ECardType
{
    NONE,
    DPS,
    HEALER
}

public enum EArmy
{
    NONE,
    PLAYER,
    MONSTER
}
