using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Utils;

public class Card : MonoBehaviour
{
    [SerializeField]
    [InlineButton("@SetCardValues()", "Init")]
    private CardSO cardData;

    #region References

    [SerializeField, FoldoutGroup("References")]
    private MeshRenderer cardBackground;

    [SerializeField, FoldoutGroup("References")]
    private SpriteRenderer spriteRenderer;

    [SerializeField, FoldoutGroup("References")]
    private Animation borderAnimation; // using animation for optimization cuz I have only basic animation, if wee need more complex animation states - use Animator

    [SerializeField, FoldoutGroup("References")]
    private TextMeshPro cardEffectValueText;

    [SerializeField, FoldoutGroup("References")]
    private TextMeshPro healthText;

    [SerializeField, FoldoutGroup("References")]
    private TextMeshPro healthChangeInfoText;

    [SerializeField, FoldoutGroup("References")]
    private Animation animation;

    [SerializeField, FoldoutGroup("References")]
    private Collider collider;

    public Collider Collider => collider;

    #endregion

    [Title("Card Values")]
    [SerializeField]
    private ECardType cardType;

    public ECardType CardType => cardType;

    [SerializeField]
    private bool isOpponentCard;

    public bool IsOpponentCard => isOpponentCard;

    [SerializeField]
    private int currentHealth;

    private bool IsDead => currentHealth <= 0;

    public int CurrentHealth => currentHealth;

    [SerializeField]
    private CardsRow currentCardsRow;

    public CardsRow CurrentCardsRow
    {
        get => currentCardsRow;
        set => currentCardsRow = value;
    }

    public bool IsInHand => CurrentCardsRow.IsInHand;

    [SerializeField]
    [ShowIf("cardType", ECardType.DPS)]
    private int damage;

    public int Damage => damage;

    [SerializeField]
    [ShowIf("cardType", ECardType.HEALER)]
    private int healValue;

    public int HealValue => healValue;

    private float speedOfCardPositionLerp = 7.5f;
    private Coroutine cardPositionLerpCoroutine;

    private Vector3 initLocalScaleCard;
    private Vector3 currentTargetPosition;

    public void Init(bool isOpponentCard)
    {
        SetCardValues();

        this.isOpponentCard = isOpponentCard;
        gameObject.layer = isOpponentCard ? LayerManager.OpponentCard : LayerManager.PlayerCard;
    }

    private void SetCardValues()
    {
        if (cardData == null) return;

        spriteRenderer.sprite = cardData.sprite;

        cardType = cardData.cardType;

        name = cardData.nameInGame;

        SetCardToFullColor();
        ChangeCardTypeBackground();

        cardEffectValueText.text =
            cardType == ECardType.DPS ? cardData.damage.ToString() : cardData.healValue.ToString();
        damage = cardData.damage;
        healValue = cardData.healValue;

        healthText.text = cardData.health.ToString();
      
        currentHealth = cardData.health;
    }

    private void ChangeCardTypeBackground()
    {
        var block = new MaterialPropertyBlock();
        switch (cardType)
        {
            case ECardType.DPS:
                block.SetColor(Config.ShaderProperties.Color, Config.Colors.CardDpsBackground);
                break;
            case ECardType.HEALER:
                block.SetColor(Config.ShaderProperties.Color, Config.Colors.CardHealerBackground);
                break;
        }

        cardBackground.SetPropertyBlock(block);
    }

    public void SetCardData(CardSO cardData)
    {
        this.cardData = cardData;
    }

    public void ReceiveDamage(int damage)
    {
        var newHealthValue = currentHealth - damage;
        currentHealth = Mathf.Clamp(newHealthValue, 0, cardData.health);
        healthText.text = currentHealth.ToString();
        StartCoroutine(HealthChangeInfo(damage, true));

        if (IsDead)
        {
            CardDefeated();
        }
        else
        {
            CardHitAnimation();
        }
    }

    private IEnumerator HealthChangeInfo(int value, bool isDamage)
    {
        if (isDamage)
        {
            healthChangeInfoText.color = Config.Colors.CardEffectValueColor;
            healthChangeInfoText.text = (-value).ToString();
        }
        else
        {
            healthChangeInfoText.color = Config.Colors.CardHealthValueColor;
            healthChangeInfoText.text = "+" + value;
        }

        healthChangeInfoText.transform.localPosition = new Vector3(-0.5f, -0.5f, 0.0f);
        var infoColor = healthChangeInfoText.color;
        infoColor.a = 1.0f;
        healthChangeInfoText.color = infoColor;

        while (healthChangeInfoText.color.a >= 0.02f)
        {
            infoColor = healthChangeInfoText.color;
            infoColor.a -= Time.deltaTime;
            healthChangeInfoText.color = infoColor;
            healthChangeInfoText.transform.localPosition += new Vector3(0.001f, 0.001f, 0.0f);

            yield return null;
        }

        infoColor.a = 0.0f;
        healthChangeInfoText.color = infoColor;
    }

    private void CardHitAnimation()
    {
        animation.Play(AnimatorHash.CardHit);
    }

    private void CardDefeated()
    {
        CurrentCardsRow.RemoveCard(this);
    }

    public void Heal(int healValue)
    {
        var missingToMaxHealth = cardData.health - currentHealth;
        missingToMaxHealth = Mathf.Clamp(missingToMaxHealth, missingToMaxHealth, healValue);
        if (missingToMaxHealth > 0)
        {
            StartCoroutine(HealthChangeInfo(missingToMaxHealth, false));
            var newHealthValue = currentHealth + healValue;
            currentHealth = Mathf.Clamp(newHealthValue, newHealthValue, cardData.health);
            healthText.text = currentHealth.ToString();
        }

        CardHealAnimation();
    }

    private void CardHealAnimation()
    {
        animation.Play(AnimatorHash.CardHeal);
    }

    public void ResetCardValues()
    {
        SetCardValues();
        CurrentCardsRow = null;
        StopAllCoroutines();
    }

    public void SetCardInGraveyard(Graveyard graveyard)
    {
        graveyard.AddCard(this);
        
        CurrentCardsRow = null; // we can't move card between rows and get back to hand;

        SetCardToGrayscale();
    }

    private void SetCardToGrayscale()
    {
        var block = new MaterialPropertyBlock();
        block.SetColor(Config.ShaderProperties.Color, Config.Colors.GrayScale);
        cardBackground.SetPropertyBlock(block);

        healthText.color = Config.Colors.GrayScale;
        cardEffectValueText.color = Config.Colors.GrayScale;

        spriteRenderer.material.SetFloat(Config.ShaderProperties.GrayScaleAmount,
            1.0f); // TODO: change to material property block, need to prepare shader for it
    }
    
    private void SetCardToFullColor()
    {
        healthText.color = Config.Colors.CardHealthValueColor;
        cardEffectValueText.color = Config.Colors.CardEffectValueColor;

        spriteRenderer.material.SetFloat(Config.ShaderProperties.GrayScaleAmount,
            0.0f); // TODO: change to material property block, need to prepare shader for it
    }

    public void MoveCardToPosition(Vector3 targetPosition)
    {
        currentTargetPosition = targetPosition;
        StopCardPositionLerp();
        cardPositionLerpCoroutine = StartCoroutine(LerpToPosition(targetPosition));
    }

    private IEnumerator LerpToPosition(Vector3 targetPosition)
    {
        while ((transform.position - targetPosition).sqrMagnitude > 0.000001f)
        {
            transform.position =
                Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * speedOfCardPositionLerp);

            yield return null;
        }

        transform.position = targetPosition;
    }

    public void MoveCardToLocalPosition(Vector3 targetPosition)
    {
        currentTargetPosition = targetPosition;
        StopCardPositionLerp();
        cardPositionLerpCoroutine = StartCoroutine(LerpToLocalPosition(targetPosition));
    }

    private IEnumerator LerpToLocalPosition(Vector3 targetPosition)
    {
        while ((transform.localPosition - targetPosition).sqrMagnitude > 0.000001f)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, targetPosition,
                Time.deltaTime * speedOfCardPositionLerp);

            yield return null;
        }

        transform.localPosition = targetPosition;
    }


    public void StopCardPositionLerp()
    {
        if (cardPositionLerpCoroutine != null)
        {
            StopCoroutine(cardPositionLerpCoroutine);
        }
    }

    public void SetCardAsActive()
    {
        //initLocalScaleCard = transform.localScale; // it's bad :(
        //animation.Play(AnimatorHash.CardActive);

        StopCardPositionLerp();
        currentTargetPosition = currentTargetPosition - new Vector3(0.0f, 0.0f, 0.9f);
        MoveCardToLocalPosition(currentTargetPosition);
    }

    public void SetCardAsInactive()
    {
        //animation.Stop();
        //transform.localScale = initLocalScaleCard; // maybe lerp?

        StopCardPositionLerp();
        currentTargetPosition = currentTargetPosition + new Vector3(0.0f, 0.0f, 0.9f);
        MoveCardToLocalPosition(currentTargetPosition);
    }

    public bool HasMaxHP()
    {
        return currentHealth == cardData.health;
    }

    public void SetReadyForDamage()
    {
        borderAnimation.gameObject.SetActive(true);
        borderAnimation.Play(AnimatorHash.CardReadyForDamage);
    }

    public void SetReadyForHeal()
    {
        borderAnimation.gameObject.SetActive(true);
        borderAnimation.Play(AnimatorHash.CardReadyForHealing);
    }

    public void SetReadyForPlayOnBoard()
    {
        borderAnimation.gameObject.SetActive(true);
        borderAnimation.Play(AnimatorHash.CardReadyForPlayOnBoard);
    }

    public void DisableBorderAnimation()
    {
        borderAnimation.gameObject.SetActive(false);
    }

    public string GetCardName()
    {
        return cardData.nameInGame;
    }

    public string GetCardDescription()
    {
        return cardData.GetFormattedDescription();
    }
}