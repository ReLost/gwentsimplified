﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gwent Army", menuName = "GwentSimplified/New Army", order = 2)]
public class ArmySO : ScriptableObject
{
   [SerializeField]
   private List<CardSO> armyUnits = new List<CardSO>();

   public CardSO GetRandomUnit()
   {
      return armyUnits[Random.Range(0, armyUnits.Count)];
   }

   public CardSO GetDPSUnit()
   {
      for (int i = 0; i < armyUnits.Count; i++)
      {
         if (armyUnits[i].cardType == ECardType.DPS)
         {
            return armyUnits[i];
         }
      }

      return armyUnits[0];
   }
}
