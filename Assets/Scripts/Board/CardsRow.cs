﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

public class CardsRow : MonoBehaviour
{
    [SerializeField]
    private Board board;

    [SerializeField]
    private MeshRenderer rowRenderer;

    [SerializeField]
    private bool isInHand;

    public bool IsInHand => isInHand;

    [SerializeField]
    private List<Card> cards = new List<Card>();

    public List<Card> Cards => cards;

    public bool AnyCardsInRow => cards.Count > 0;

    private const float CARD_OFFSET = -0.05f;

    private Coroutine colorFading;

    public void AddCard(Card card)
    {
        card.transform.SetParent(transform);

        AddCardToList(card);

        if (card.CurrentCardsRow != null && card.CurrentCardsRow.isInHand)
        {
            card.CurrentCardsRow.RemoveCardFromHand(card);
        }

        card.CurrentCardsRow = this;

        SetCardsPosition();
    }

    private void AddCardToList(Card card)
    {
        if (card.IsOpponentCard || cards.Count == 0)
        {
            cards.Add(card);
            return;
        }

        var leftBorderFirstCard = cards.Count * CARD_OFFSET - CARD_OFFSET;
        var rightBorderLastCard = leftBorderFirstCard * -1;

        if (card.transform.localPosition.x < leftBorderFirstCard)
        {
            cards.Insert(0, card);
        }
        else if (card.transform.localPosition.x > rightBorderLastCard)
        {
            cards.Add(card);
        }
        else
        {
            for (int i = 0; i < cards.Count - 1; i++)
            {
                var firstCard = cards[i].transform.localPosition.x;
                var nextCard = cards[i + 1].transform.localPosition.x;

                if (card.transform.localPosition.x > firstCard &&
                    card.transform.localPosition.x < nextCard)
                {
                    cards.Insert(i + 1, card);
                    return;
                }
            }
        }
    }

    private void SetCardsPosition()
    {
        var leftBorderFirstCard = cards.Count * CARD_OFFSET - CARD_OFFSET;

        for (int i = 0; i < cards.Count; i++)
        {
            var newPosition = new Vector3(leftBorderFirstCard - CARD_OFFSET * 2 * i, 0.1f, -0.1f);
            cards[i].MoveCardToLocalPosition(newPosition);
        }
    }

    public void RemoveCard(Card card)
    {
        cards.Remove(card);

        card.SetCardInGraveyard(board.Graveyard);

        SetCardsPosition();
    }

    private void RemoveCardFromHand(Card card)
    {
        cards.Remove(card);
        SetCardsPosition();
    }

    public void ClearRowAndReturnCardsToPool(Transform cardsPool)
    {
        for (var i = 0; i < cards.Count; i++)
        {
            var card = cards[i];
            card.ResetCardValues();
            card.transform.SetParent(cardsPool);
            card.transform.position = Vector3.one * 100.0f;
        }

        cards.Clear();
    }

    public void SetWoundedPlayerCardsFromBoardReadyForHeal()
    {
        for (var i = 0; i < Cards.Count; i++)
        {
            var card = Cards[i];
            if (card.HasMaxHP() == false)
            {
                card.SetReadyForHeal();
            }
        }
    }

    public void SetPlayerCardsFromHandReadyToPlay()
    {
        for (var i = 0; i < Cards.Count; i++)
        {
            var card = Cards[i];
            card.SetReadyForPlayOnBoard();
        }
    }

    public void SetOpponentCardsFromBoardReadyForDamage()
    {
        for (var i = 0; i < Cards.Count; i++)
        {
            var card = Cards[i];
            card.SetReadyForDamage();
        }
    }

    public bool AnyCardNeedHeal()
    {
        for (var i = 0; i < cards.Count; i++)
        {
            var card = cards[i];
            if (card.HasMaxHP() == false)
            {
                return true;
            }
        }

        return false;
    }

    public void DisableBorderAnimationOnCards()
    {
        for (var i = 0; i < Cards.Count; i++)
        {
            var card = Cards[i];
            card.DisableBorderAnimation();
        }
    }

    public List<Card> GetWoundedOpponentCardFromBoard()
    {
        var woundedCards = new List<Card>();
        for (var i = 0; i < Cards.Count; i++)
        {
            var card = Cards[i];
            if (card.HasMaxHP() == false)
            {
                woundedCards.Add(card);
            }
        }

        return woundedCards;
    }

    public int GetPointsFromRow()
    {
        var totalPoints = 0;
        for (int i = 0; i < Cards.Count; i++)
        {
            totalPoints += Cards[i].CurrentHealth;
        }

        return totalPoints;
    }

    public void SetRowAsActive()
    {
        if (rowRenderer == null) return;

        var block = new MaterialPropertyBlock();
        colorFading = StartCoroutine(RowColorFade(0.2f));

        IEnumerator RowColorFade(float speedOfFading)
        {
            while (true)
            {
                var colorValue = Mathf.PingPong(Time.time * speedOfFading, 0.2f);
                block.SetColor(Config.ShaderProperties.Color, new Color(1.0f - colorValue, 1.0f, 1.0f - colorValue));
                rowRenderer.SetPropertyBlock(block);
                yield return null;
            }
        }
    }

    public void SetRowAsInactive()
    {
        if (colorFading != null)
        {
            StopCoroutine(colorFading);
        }

        var block = new MaterialPropertyBlock();
        block.SetColor(Config.ShaderProperties.Color, new Color(1.0f, 1.0f, 1.0f));
        rowRenderer.SetPropertyBlock(block);
    }
    
    public Card GetRandomCard()
    {
        return AnyCardsInRow ? cards[Random.Range(0, cards.Count)] : null;
    }
}