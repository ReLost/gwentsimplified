﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graveyard : MonoBehaviour
{
    [SerializeField]
    private List<Card> cards = new List<Card>();

    public List<Card> Cards => cards;


    public void AddCard(Card card)
    {
        cards.Add(card);
        card.transform.SetParent(transform);
        var heightOfStack = Cards.Count * 0.1f;
        var positionOnGraveyardStack = new Vector3(0.0f, heightOfStack, 0.0f);
        card.MoveCardToLocalPosition(positionOnGraveyardStack);
    }

    public void RemoveCard(Card card, Transform cardsPool)
    {
        card.ResetCardValues();
        card.transform.SetParent(cardsPool);
        card.transform.position = Vector3.one * 100.0f;
        cards.Remove(card);
    }

    public void RemoveAllCards(Transform cardsPool)
    {
        for (var i = 0; i < cards.Count; i++)
        {
            var card = cards[i];
            RemoveCard(card, cardsPool);
        }
        
        Cards.Clear();
    }
    
    public void DisableBorderAnimationOnCards()
    {
        for (var i = 0; i < Cards.Count; i++)
        {
            var card = Cards[i];
            card.DisableBorderAnimation();
        }
    }
}

