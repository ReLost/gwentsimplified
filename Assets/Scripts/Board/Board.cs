﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class Board : MonoBehaviour
{
    [SerializeField] private Graveyard graveyard;

    public Graveyard Graveyard => graveyard;

    [Title("Player")] [SerializeField] private CardsRow playersHandRow;

    public CardsRow PlayersHandRow => playersHandRow;

    [SerializeField] private CardsRow playersFirstRow;

    [SerializeField] private CardsRow playersSecondRow;

    [Title("Opponent")] [SerializeField] private CardsRow opponentHandRow;

    public CardsRow OpponentHandRow => opponentHandRow;

    [SerializeField] private CardsRow opponentsFirstRow;

    [SerializeField] private CardsRow opponentsSecondRow;

    public void ClearBoardAndReturnCardsToPool(Transform cardsPool)
    {
        playersHandRow.ClearRowAndReturnCardsToPool(cardsPool);
        playersFirstRow.ClearRowAndReturnCardsToPool(cardsPool);
        playersSecondRow.ClearRowAndReturnCardsToPool(cardsPool);

        opponentHandRow.ClearRowAndReturnCardsToPool(cardsPool);
        opponentsFirstRow.ClearRowAndReturnCardsToPool(cardsPool);
        opponentsSecondRow.ClearRowAndReturnCardsToPool(cardsPool);

        graveyard.RemoveAllCards(cardsPool);
    }


    #region Player Cards

    public bool AnyCardsOnPlayerHalf()
    {
        return playersFirstRow.AnyCardsInRow || playersSecondRow.AnyCardsInRow;
    }

    public bool AnyPlayerCardNeedHeal()
    {
        return playersFirstRow.AnyCardNeedHeal() || playersSecondRow.AnyCardNeedHeal();
    }

    public CardsRow GetRandomPlayerRow()
    {
        return Random.value > 0.5f ? playersFirstRow : playersSecondRow;
    }

    public Card GetRandomPlayerCardFromBoard()
    {
        var allCardsOnPlayerHalf = new List<Card>();
        allCardsOnPlayerHalf.AddRange(playersFirstRow.Cards);
        allCardsOnPlayerHalf.AddRange(playersSecondRow.Cards);
        return allCardsOnPlayerHalf[Random.Range(0, allCardsOnPlayerHalf.Count)];
    }

    public void SetAllWoundedPlayerCardsFromBoardReadyForHeal()
    {
        playersFirstRow.SetWoundedPlayerCardsFromBoardReadyForHeal();

        playersSecondRow.SetWoundedPlayerCardsFromBoardReadyForHeal();
    }

    public bool AnyCardInPlayerHand()
    {
        return playersHandRow.AnyCardsInRow;
    }

    public void SetAllPlayerCardsFromHandReadyToPlay()
    {
        playersHandRow.SetPlayerCardsFromHandReadyToPlay();
    }

    public int GetPlayerPoints()
    {
        var totalPoints = 0;
        totalPoints += playersFirstRow.GetPointsFromRow();
        totalPoints += playersSecondRow.GetPointsFromRow();

        return totalPoints;
    }

    #endregion

    #region Opponent Card

    public bool AnyCardsOnOpponentHalf()
    {
        return opponentsFirstRow.AnyCardsInRow || opponentsSecondRow.AnyCardsInRow;
    }

    public bool AnyOpponentCardNeedHeal()
    {
        return opponentsFirstRow.AnyCardNeedHeal() || opponentsSecondRow.AnyCardNeedHeal();
    }

    public Card GetRandomCardFromOpponentHand()
    {
        return OpponentHandRow.GetRandomCard();
    }

    public CardsRow GetRandomOpponentRow()
    {
        return Random.value > 0.5f ? opponentsFirstRow : opponentsSecondRow;
    }

    public Card GetRandomOpponentCardFromBoard()
    {
        var allCardsOnOpponentHalf = new List<Card>();
        allCardsOnOpponentHalf.AddRange(opponentsFirstRow.Cards);
        allCardsOnOpponentHalf.AddRange(opponentsSecondRow.Cards);
        return allCardsOnOpponentHalf[Random.Range(0, allCardsOnOpponentHalf.Count)];
    }

    public void SetAllOpponentCardsFromBoardReadyForDamage()
    {
        opponentsFirstRow.SetOpponentCardsFromBoardReadyForDamage();

        opponentsSecondRow.SetOpponentCardsFromBoardReadyForDamage();
    }

    public List<Card> GetAllWoundedOpponentCardFromBoard()
    {
        var allWoundedCardsOnOpponentHalf = new List<Card>();
        allWoundedCardsOnOpponentHalf.AddRange(opponentsFirstRow.GetWoundedOpponentCardFromBoard());
        allWoundedCardsOnOpponentHalf.AddRange(opponentsSecondRow.GetWoundedOpponentCardFromBoard());

        return allWoundedCardsOnOpponentHalf;
    }

    public Card GetRandomWoundedOpponentCardFromBoard()
    {
        var allWoundedCardsOnOpponentHalf = new List<Card>();
        allWoundedCardsOnOpponentHalf.AddRange(opponentsFirstRow.GetWoundedOpponentCardFromBoard());
        allWoundedCardsOnOpponentHalf.AddRange(opponentsSecondRow.GetWoundedOpponentCardFromBoard());

        return allWoundedCardsOnOpponentHalf[Random.Range(0, allWoundedCardsOnOpponentHalf.Count)];
    }

    public bool AnyCardInOpponentHand()
    {
        return opponentHandRow.AnyCardsInRow;
    }

    public int GetOpponentPoints()
    {
        var totalPoints = 0;

        totalPoints += opponentsFirstRow.GetPointsFromRow();
        totalPoints += opponentsSecondRow.GetPointsFromRow();

        return totalPoints;
    }

    #endregion

    public bool AnyCardsInHands()
    {
        return AnyCardInOpponentHand() || AnyCardInPlayerHand();
    }

    public void DisableAllBorderAnimationOnCards()
    {
        playersHandRow.DisableBorderAnimationOnCards();
        playersFirstRow.DisableBorderAnimationOnCards();
        playersSecondRow.DisableBorderAnimationOnCards();

        opponentHandRow.DisableBorderAnimationOnCards();
        opponentsFirstRow.DisableBorderAnimationOnCards();
        opponentsSecondRow.DisableBorderAnimationOnCards();

        graveyard.DisableBorderAnimationOnCards();
    }

    public void SetPlayerRowsAsActive()
    {
        playersFirstRow.SetRowAsActive();
        playersSecondRow.SetRowAsActive();
    }

    public void SetPlayerRowsAsInactive()
    {
        playersFirstRow.SetRowAsInactive();
        playersSecondRow.SetRowAsInactive();
    }
}