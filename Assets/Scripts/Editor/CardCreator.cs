﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

public class CardCreator : OdinEditorWindow
{
    [MenuItem("Gwent/Card Creator...")]
    private static void OpenWindow()
    {
        var window = GetWindow<CardCreator>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(600, 800);
    }
    
    [OnInspectorGUI("DrawPreview", append: true)]
    [HideLabel, ReadOnly]
    public Texture2D Texture;

    private void DrawPreview()
    {
        if (this.Texture == null) return;

        GUILayout.BeginVertical(GUI.skin.box);
        var guiStyle = new GUIStyle {alignment = TextAnchor.MiddleCenter};
        GUILayout.Label(this.Texture, guiStyle);
        GUILayout.EndVertical();
    }
    
    [Title("Gwent Card Creator", TitleAlignment = TitleAlignments.Centered)]
    [OnValueChanged("OnAnyValueChanged")]
    public string ID;

    [OnValueChanged("OnAnyValueChanged")]
    public string nameInGame;
    
    [OnValueChanged("OnAnyValueChanged")]
    public EArmy army;

    [OnValueChanged("OnAnyValueChanged")]
    public ECardType cardType;

    [OnValueChanged("OnAnyValueChanged")]
    [InlineEditor(InlineEditorModes.LargePreview)]
    public Sprite sprite;
    
    [OnValueChanged("OnAnyValueChanged")]
    [Multiline]
    public string tooltipInfo;

    [OnValueChanged("OnAnyValueChanged")]
    public int health;

    [OnValueChanged("OnAnyValueChanged")]
    [ShowIf("cardType", ECardType.DPS)]
    public int damage;

    [OnValueChanged("OnAnyValueChanged")]
    [ShowIf("cardType", ECardType.HEALER)]
    public int healValue;

    [SerializeField, ReadOnly, HideLabel, GUIColor("GetColor"), Multiline(2)]
    [Title("Result Info")]
    private string infoBoxMessage;

    private Color infoColor = Color.gray;
    
    public Color GetColor()
    {
        return infoColor;
    }
    
    private void OnAnyValueChanged()
    {
        infoColor = Color.gray;
        infoBoxMessage = "";
    }

    [Button(ButtonSizes.Gigantic)]
    [EnableIf("AllDataAvailable")]
    private void CreateCard()
    {
        try
        {
            CardSO asset = CreateCardInstance();

            var path = GetPath();

            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();

            Selection.activeObject = asset;

            ClearData();
            
            ResultSuccessInfo(path);
        }
        catch
        {
            ResultFailureInfo();
        }
    }

    private CardSO CreateCardInstance()
    {
        CardSO card = CreateInstance<CardSO>();
        card.ID = ID;
        card.nameInGame = nameInGame;
        card.army = army;
        card.cardType = cardType;
        card.health = health;
        card.damage = damage;
        card.healValue = healValue;
        card.sprite = sprite;
        card.description = tooltipInfo;

        return card;
    }

    private string GetPath()
    {
        switch (army)
        {
            case EArmy.PLAYER:
                return "Assets/Prefabs/Cards Objects/Player Army/" + nameInGame + ".asset";
            case EArmy.MONSTER:
                return "Assets/Prefabs/Cards Objects/Monster Army/" + nameInGame + ".asset";
            default:
                return string.Empty;
        }
    }

    private void ClearData()
    {
        ID = string.Empty;
        nameInGame = string.Empty;
        cardType = ECardType.NONE;
        sprite = null;
        damage = 0;
        health = 0;
        healValue = 0;
    }

    private void ResultSuccessInfo(string filePath)
    {
        infoColor = Color.green;
        infoBoxMessage = "Success! \nNew card created in " + filePath;
    }
    
    private void ResultFailureInfo()
    {
        infoColor = Color.red;
        infoBoxMessage = "Failure! \nPlease contact with any programmer!";
    }

    private bool AllDataAvailable()
    {
        return string.IsNullOrEmpty(ID) == false && string.IsNullOrEmpty(nameInGame) == false &&
               cardType != ECardType.NONE && army != EArmy.NONE &&  sprite != null && health > 0 &&
               (cardType == ECardType.DPS && damage > 0 || cardType == ECardType.HEALER && healValue > 0);
    }
}