﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

public class CardCollection : OdinMenuEditorWindow
{
    [MenuItem("Gwent/Card Collection")]
    private static void OpenWindow()
    {
        var window = GetWindow<CardCollection>();
        window.position = GUIHelper.GetEditorWindowRect().AlignCenter(1200, 800);
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        var tree = new OdinMenuTree(true);

        var customMenuStyle = new OdinMenuStyle
        {
            BorderPadding = 0f,
            AlignTriangleLeft = true,
            TriangleSize = 16f,
            TrianglePadding = 0f,
            Offset = 20f,
            Height = 23,
            IconPadding = 0f,
            BorderAlpha = 0.323f
        };

        tree.DefaultMenuStyle = customMenuStyle;
        tree.Config.DrawSearchToolbar = true;
        tree.AddAllAssetsAtPath("Player Army", "Assets/Prefabs/Cards Objects/Player Army", typeof(ScriptableObject), true, true);
        tree.AddAllAssetsAtPath("Monster Army", "Assets/Prefabs/Cards Objects/Monster Army", typeof(ScriptableObject), true, true);

        tree.EnumerateTree()
            .AddThumbnailIcons()
            .SortMenuItemsByName();

        return tree;
    }
}
