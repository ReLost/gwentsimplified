﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI cardName;

    [SerializeField]
    private TextMeshProUGUI description;


    public void ShowTooltip(string cardName, string description)
    {
        this.cardName.text = cardName;
        this.description.text = description;
        transform.position = Input.mousePosition;
        gameObject.SetActive(true);
    }

    public void HideTooltip()
    {
        gameObject.SetActive(false);
    }
}
