﻿using UnityEngine;

namespace Utils
{
    public static class AnimatorHash
    {
        #region Hashed Animator Parameters

        // optimized hashed animator parameters
        public static int Play = Animator.StringToHash("Play");

        #endregion

        #region Animation Names

        public static string CardHit = "CardHit";
        
        public static string CardHeal = "CardHeal";
        
        public static string CardReadyForDamage = "CardReadyForDamage";
        
        public static string CardReadyForHealing = "CardReadyForHealing";
        
        public static string CardReadyForPlayOnBoard = "CardReadyForPlayOnBoard";

        public static string CardActive = "CardActive";

        #endregion
    }
}