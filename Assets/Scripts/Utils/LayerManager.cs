﻿using UnityEngine;

namespace Utils
{
    public static class LayerManager
    {
        public static readonly int Default = LayerMask.NameToLayer("Default");    
    
        public static readonly int PlayerCard = LayerMask.NameToLayer("PlayerCard");
    
        public static readonly int OpponentCard = LayerMask.NameToLayer("OpponentCard");
    
        public static readonly int PlayerCardsRow = LayerMask.NameToLayer("PlayerCardsRow");
    
        public static readonly int OpponentCardsRow = LayerMask.NameToLayer("OpponentCardsRow");

        public static readonly int AnyCard = PlayerCard | OpponentCard;
    
        public static readonly int AnyCardsRow = PlayerCardsRow | OpponentCardsRow;

        public static readonly int AnyGameplayLayer = AnyCardsRow | AnyCard;
    
        public struct BitLayer
        {
            public static readonly int Default = 1 << LayerMask.NameToLayer("Default");    
        
            public static readonly int PlayerCard = 1 << LayerMask.NameToLayer("PlayerCard");
        
            public static readonly int OpponentCard = 1 << LayerMask.NameToLayer("OpponentCard");
    
            public static readonly int PlayerCardsRow = 1 << LayerMask.NameToLayer("PlayerCardsRow");
    
            public static readonly int OpponentCardsRow = 1 << LayerMask.NameToLayer("OpponentCardsRow");

            public static readonly int AnyCard = PlayerCard | OpponentCard;

            public static readonly int AnyCardsRow = PlayerCardsRow | OpponentCardsRow;
        
            public static readonly int AnyGameplayLayer = AnyCard | AnyCardsRow;

        }

        public static bool CompareLayer(this GameObject gameObject, int layerID)
        {
            return gameObject.layer == layerID;
        }
    
        public static bool CompareLayer(this Collider collider, int layerID)
        {
            return collider.gameObject.layer == layerID;
        }
    
        public static bool CompareLayer(this Card card, int layerID)
        {
            return card.gameObject.layer == layerID;
        }
    }
}
