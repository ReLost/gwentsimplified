﻿using UnityEngine;

namespace Utils
{
    public static class Config
    {
        public static class ShaderProperties
        {
            public static int Color = Shader.PropertyToID("_Color");

            public static int GrayScaleAmount = Shader.PropertyToID("_EffectAmount");
        }

        public static class Colors
        {
            public static Color CardDpsBackground = new Color(0.5f, 0.0f, 0.0f);
            public static Color CardHealerBackground = new Color(0.0f, 0.5f, 0.0f);

            public static Color CardEffectValueColor = Color.red;
            public static Color CardHealthValueColor = Color.green;
        
            public static Color GrayScale = new Color(0.2f, 0.2f, 0.2f);
        }
    }
}
