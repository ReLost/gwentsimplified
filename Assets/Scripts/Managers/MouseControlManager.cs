﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Utils;

public class MouseControlManager : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private UIManager uiManager;

    [SerializeField]
    private Camera camera;

    private Vector3 initCardPosition = Vector3.zero;

    private Card draggingCard;
    private bool isDragging;

    private Card activeCard;

    // Update is called once per frame
    void Update()
    {
        if (gameManager.PlayersTurn)
        {
            if (Input.GetMouseButtonDown(0))
            {
                TrySelectCard();
            }

            if (Input.GetMouseButtonUp(0))
            {
                TryPutCardOnBoard();
            }

            if (isDragging)
            {
                DragCard();
            }
            
            if (Input.GetMouseButtonDown(1))
            {
                TryShowTooltip();
            }
            
            if (Input.GetMouseButtonUp(1))
            {
               HideTooltip();
            }
        }
    }


    private void TrySelectCard()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit, Mathf.Infinity, LayerManager.BitLayer.AnyCard))
        {
            if (gameManager.PutCardOnBoard == false)
            {
                var card = hit.transform.GetComponent<Card>();
                if (card.CompareLayer(LayerManager.PlayerCard) && card.CurrentCardsRow != null && card.IsInHand)
                {
                    GetCardFromHand(card);
                }
            }
            else
            {
                TryUseCardAbility(hit);
            }
        }
    }

    private void GetCardFromHand(Card card)
    {
        draggingCard = card;
        draggingCard.StopCardPositionLerp();
        initCardPosition = draggingCard.transform.position;
        gameManager.DisableBorderAnimationOnAllCards();
        gameManager.SetPlayerRowsAsActive();
        isDragging = true;
    }

    private void TryUseCardAbility(RaycastHit hit)
    {
        var hitCard = hit.transform.GetComponent<Card>();
        if (hitCard.CurrentCardsRow != null && hitCard.IsInHand == false)
        {
            switch (activeCard.CardType)
            {
                case ECardType.DPS when hitCard.IsOpponentCard:
                    hitCard.ReceiveDamage(activeCard.Damage);
                    gameManager.AllowOpponentTurn();
                    activeCard.SetCardAsInactive();
                    activeCard = null;
                    gameManager.DisableBorderAnimationOnAllCards();
                    uiManager.ChangeIconToNormal();
                    UpdatePointsInfo();
                    break;
                case ECardType.HEALER when hitCard.IsOpponentCard == false && hitCard.HasMaxHP() == false:
                    hitCard.Heal(activeCard.HealValue);
                    gameManager.AllowOpponentTurn();
                    activeCard.SetCardAsInactive();
                    activeCard = null;
                    gameManager.DisableBorderAnimationOnAllCards();
                    uiManager.ChangeIconToNormal();
                    UpdatePointsInfo();
                    break;
            }
        }
    }

    private void TryPutCardOnBoard()
    {
        if (draggingCard != null)
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            draggingCard.Collider.enabled = false; // prevent to raycast hit current playing card

            if (Physics.Raycast(ray, out var hit, Mathf.Infinity, LayerManager.BitLayer.PlayerCardsRow))
            {
                if (draggingCard.IsInHand)
                {
                    var cardsRow = hit.transform.GetComponent<CardsRow>();
                    PutCardOnBoard(cardsRow);
                }
            }

            if (initCardPosition != Vector3.zero)
            {
                CardGetBackToHand();
            }

            draggingCard.Collider.enabled = true;
            isDragging = false;
            draggingCard = null;
        }
    }

    private void PutCardOnBoard(CardsRow cardsRow)
    {
        cardsRow.AddCard(draggingCard);
        initCardPosition = Vector3.zero;
        if (draggingCard.CardType == ECardType.DPS && gameManager.AnyCardOnEnemyHalf() ||
            draggingCard.CardType == ECardType.HEALER && gameManager.AnyPlayerCardNeedHeal())
        {
            gameManager.PutCardOnBoard = true;

            activeCard = draggingCard;
            activeCard.SetCardAsActive();
            switch (draggingCard.CardType)
            {
                case ECardType.DPS:
                    uiManager.ChangeIconToDamage();
                    gameManager.SetOpponentCardsReadyForDamage();
                    break;
                case ECardType.HEALER:
                    uiManager.ChangeIconToHeal();
                    gameManager.SetPlayerCardsReadyForHeal();
                    break;
            }
        }
        else
        {
            gameManager.AllowOpponentTurn();
        }

        gameManager.SetPlayerRowsAsInactive();
        UpdatePointsInfo();
    }

    private void UpdatePointsInfo()
    {
        gameManager.UpdatePointsInfo();
    }

    private void CardGetBackToHand()
    {
        gameManager.SetAllPlayerCardsFromHandReadyToPlay();
        gameManager.SetPlayerRowsAsInactive();
        draggingCard.MoveCardToPosition(initCardPosition);
        initCardPosition = Vector3.zero;
    }

    private void DragCard()
    {
        var worldPointPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        var newCardPosition = new Vector3(worldPointPosition.x, 0.2f, worldPointPosition.z);
        draggingCard.MoveCardToPosition(newCardPosition);
    }

    private void TryShowTooltip( )
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out var hit, Mathf.Infinity, LayerManager.BitLayer.PlayerCard))
        {
            var card = hit.collider.GetComponent<Card>();
            ShowTooltip(card);
        }
       
    }

    private void ShowTooltip(Card card)
    {
        uiManager.Tooltip.ShowTooltip(card.GetCardName(), card.GetCardDescription());
    }
    
    private void HideTooltip()
    {
        uiManager.Tooltip.HideTooltip();
    }
}