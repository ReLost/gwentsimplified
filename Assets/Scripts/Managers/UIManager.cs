﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Utils;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Camera camera;

    [SerializeField]
    private TextMeshProUGUI turnInfo;

    [SerializeField]
    private Transform arrow;

    private Transform arrowParent;

    [SerializeField]
    private Tooltip tooltip;

    public Tooltip Tooltip => tooltip;

    [SerializeField]
    [FoldoutGroup("Player Points Info")]
    private TextMeshProUGUI playerPoints;

    [SerializeField]
    [FoldoutGroup("Player Points Info")]
    private TextMeshProUGUI opponentPoints;

    [SerializeField, FoldoutGroup("Cursors")]
    private Texture2D damageCursor;

    [SerializeField, FoldoutGroup("Cursors")]
    private Texture2D healCursor;

    public void ShowTurnInfo(string info)
    {
        StartCoroutine(ShowTurnInfoCoroutine(info));
    }

    private IEnumerator ShowTurnInfoCoroutine(string info)
    {
        turnInfo.text = info;
        turnInfo.color = Color.white;
        turnInfo.gameObject.SetActive(true);

        yield return new WaitForSeconds(1.5f);

        while (turnInfo.color.a >= 0.02f)
        {
            var infoColor = turnInfo.color;
            infoColor.a -= Time.deltaTime;
            turnInfo.color = infoColor;

            yield return null;
        }

        turnInfo.gameObject.SetActive(false);
    }

    public void ShowEndGameInfo(bool playerWin)
    {
        StopAllCoroutines();
        turnInfo.gameObject.SetActive(true);
        turnInfo.text = playerWin ? "Victory! \n Press <b>R</b> to restart" : "Defeated! \n Press <b>R</b> to restart";
        turnInfo.color = playerWin ? Color.yellow : Color.red;
    }

    public void ClearUI()
    {
        turnInfo.gameObject.SetActive(false);
        Tooltip.HideTooltip();
    }

    public void ShowArrow(Transform startPoint)
    {
        arrowParent = startPoint;
        arrow.gameObject.SetActive(true);
    }

    public void HideArrow()
    {
        arrow.gameObject.SetActive(true);
    }

    void Update()
    {
        UpdateTooltipPosition();
    }

    private void UpdateTooltipPosition()
    {
        if (tooltip.gameObject.activeInHierarchy)
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out var hit, Mathf.Infinity, LayerManager.BitLayer.PlayerCard) == false)
            {
                Tooltip.HideTooltip();
            }
            
            var tooltipPosition = Input.mousePosition;
            tooltip.transform.position = tooltipPosition;
        }
    }

    private void UpdateArrow()
    {
        // if (arrow.gameObject.activeInHierarchy)
        // {
        //     arrow.position = arrowParent.position;
        //
        //     var worldPointPosition = camera.ScreenToWorldPoint(Input.mousePosition);
        //     var newArrowLookRotation = new Vector3(worldPointPosition.x, 1.0f, worldPointPosition.z);
        //     var lookPos = newArrowLookRotation - arrow.position;
        //
        //     var lookRotation = Quaternion.LookRotation(lookPos);
        //     arrow.rotation = Quaternion.Euler(90.0f, 0.0f, lookRotation.eulerAngles.y);
        // }
    }

    public void ChangeIconToDamage()
    {
        Cursor.SetCursor(damageCursor, Vector2.zero, CursorMode.Auto);
    }

    public void ChangeIconToHeal()
    {
        Cursor.SetCursor(healCursor, new Vector2(0.5f, 0.5f), CursorMode.Auto);
    }

    public void ChangeIconToNormal()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void SetPlayerPoints(int points)
    {
        playerPoints.text = "Your Points: " + points;
    }

    public void SetOpponentPoints(int points)
    {
        opponentPoints.text = "Opponent Points: " + points;
    }
}