﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Board board;

    [SerializeField]
    private UIManager uiManager;

    [SerializeField]
    private Transform cardPool;

    [SerializeField]
    private List<Card> cardsInPool = new List<Card>();

    private bool isPlaying;

    private bool playersTurn;

    public bool PlayersTurn
    {
        get => playersTurn;
        set => playersTurn = value;
    }

    private bool putCardOnBoard;

    public bool PutCardOnBoard
    {
        get => putCardOnBoard;
        set => putCardOnBoard = value;
    }

    private bool enemyTurn;

    [Title("Current Game")]
    [SerializeField]
    [Space(10)]
    private ArmySO playersArmy;

    private bool playerHaveAnyDPS;

    [SerializeField]
    private ArmySO opponentsArmy;

    private bool opponentHaveAnyDPS;

    [SerializeField]
    private int numOfCards;


    private void Start()
    {
        StartCoroutine(InitGame());
    }

    private IEnumerator InitGame()
    {
        FillCardsPool();
        UpdatePointsInfo();

        yield return new WaitForSeconds(0.5f);

        for (int i = 0; i < numOfCards; i++)
        {
            var playerRandomUnit = playersArmy.GetRandomUnit(); // TODO: prevent of only healers army?
            var playersCard = cardsInPool[i];
            playersCard.transform.position = new Vector3(100.0f, 100.0f, 100.0f);
            playersCard.SetCardData(playerRandomUnit);
            playersCard.Init(false);
            board.PlayersHandRow.AddCard(playersCard);

            var opponentRandomUnit = opponentsArmy.GetRandomUnit();
            var opponentsCard = cardsInPool[cardsInPool.Count - 1 - i];
            opponentsCard.transform.position = new Vector3(100.0f, 100.0f, -100.0f);
            opponentsCard.SetCardData(opponentRandomUnit);
            opponentsCard.Init(true);
            board.OpponentHandRow.AddCard(opponentsCard);

            yield return new WaitForSeconds(0.2f);
        }

        yield return new WaitForSeconds(0.5f);

        AllowPlayerTurn();
        isPlaying = true;
    }

    private void FillCardsPool()
    {
        while (cardsInPool.Count < numOfCards * 2)
        {
            var newCard = Instantiate(cardsInPool[0], cardsInPool[0].transform.parent);
            cardsInPool.Add(newCard);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }

        if (isPlaying && enemyTurn)
        {
            StartCoroutine(EnemyTurn());
            enemyTurn = false;
        }
    }

    private void RestartGame()
    {
        isPlaying = false;
        uiManager.ClearUI();
        board.ClearBoardAndReturnCardsToPool(cardPool);
        UpdatePointsInfo();
        playerHaveAnyDPS = false;
        opponentHaveAnyDPS = false;
        StartCoroutine(InitGame());
    }

    private IEnumerator EnemyTurn()
    {
        yield return new WaitForSeconds(1.0f);

        var card = board.GetRandomCardFromOpponentHand();
        if (card == null)
        {
            AllowPlayerTurn();
            UpdatePointsInfo();
            yield break;
        }
        
        var cardsRow = board.GetRandomOpponentRow();

        cardsRow.AddCard(card);
        card.SetCardAsActive();
        UpdatePointsInfo();

        yield return new WaitForSeconds(1.0f);

        switch (card.CardType)
        {
            case ECardType.DPS when board.AnyCardsOnPlayerHalf():
                var playerCard = board.GetRandomPlayerCardFromBoard();
                playerCard.ReceiveDamage(card.Damage);
                break;

            case ECardType.HEALER when board.AnyOpponentCardNeedHeal():
                var opponentCard = board.GetRandomWoundedOpponentCardFromBoard();
                opponentCard.Heal(card.HealValue);
                break;
        }
        
        card.SetCardAsInactive();
        AllowPlayerTurn();
        UpdatePointsInfo();

    }

    public bool AnyCardOnEnemyHalf()
    {
        return board.AnyCardsOnOpponentHalf();
    }

    public bool AnyPlayerCardNeedHeal()
    {
        return board.AnyPlayerCardNeedHeal();
    }

    public void AllowOpponentTurn()
    {
        if (CheckWinCondition())
        {
            EndGame();
        }
        else
        {
            PlayersTurn = false;
            enemyTurn = true;
            putCardOnBoard = false;
            uiManager.ShowTurnInfo("Opponents turn");
        }
    }

    private void AllowPlayerTurn()
    {
        if (CheckWinCondition())
        {
            EndGame();
        }
        else
        {
            PlayersTurn = true;
            SetAllPlayerCardsFromHandReadyToPlay();
            uiManager.ShowTurnInfo("Your turn");
        }
    }

    private bool CheckWinCondition()
    {
        return board.AnyCardsInHands() == false;
    }

    private void EndGame()
    {
        isPlaying = false;
        playersTurn = false;
        enemyTurn = false;

        if (board.GetPlayerPoints() >= board.GetOpponentPoints())
        {
            uiManager.ShowEndGameInfo(true);
        }
        else
        {
            uiManager.ShowEndGameInfo(false);
        }
    }

    public void SetOpponentCardsReadyForDamage()
    {
        board.SetAllOpponentCardsFromBoardReadyForDamage();
    }
    
    public void SetPlayerCardsReadyForHeal()
    {
        board.SetAllWoundedPlayerCardsFromBoardReadyForHeal();
    }

    public void DisableBorderAnimationOnAllCards()
    {
        board.DisableAllBorderAnimationOnCards();
    }

    public void SetAllPlayerCardsFromHandReadyToPlay()
    {
        board.SetAllPlayerCardsFromHandReadyToPlay();
    }
    
    public void SetPlayerRowsAsActive()
    {
      board.SetPlayerRowsAsActive();
    }
    
    public void SetPlayerRowsAsInactive()
    {
       board.SetPlayerRowsAsInactive();
    }

    public int GetPlayerPoints()
    {
        return board.GetPlayerPoints();
    }
    
    public int GetOpponentPoints()
    {
        return board.GetOpponentPoints();
    }
    
    private void UpdatePlayerPoints()
    {
        var playerPoints = GetPlayerPoints();
        uiManager.SetPlayerPoints(playerPoints);
    }
    
    private void UpdateOpponentPoints()
    {
        var opponentPoints = GetOpponentPoints();
        uiManager.SetOpponentPoints(opponentPoints);
    }

    public void UpdatePointsInfo()
    {
        UpdatePlayerPoints();
        UpdateOpponentPoints();
    }
}